HangarOpsToSlave Version 2.0 - README
=====================================

The HangarOpsToSlave plugin works with STMA's Hangar Ops plugin. The purpose of 
the HangarOpsToSlave plugin is to transmit the custom STMA datarefs related to
key codes and hangar door positions to a slave X-Plane computer designated for
external visuals.

Users running visuals on a separate computer will likely have the same airport
scenery installed on the master and slave. With STMA's Hangar Ops plugin
installed on both master and slave, opening/closing a STMA hangar door on the
master will not be synced on the slave. The HangarOpsToSlave plugin enables
the missing sync so the hangar doors on the slave follow the master
automatically.

The plugin supports multiple slave IPs. Each slave IP must be entered in the
plugin configuration file (slaveIP.ini) one IP address per line. Loopback
addresses and invalid IPs are ignored.

It is also possible to use the plugin in a multiplayer scenario over the
Internet. In this case, slaveIP.ini must include the public IP address of the
other player's router and that router must port-forward packets arriving on
port 49000 to one or more private IPs running X-Plane on the private LAN side
of the router.

To see the UDP packets being sent to Slave IPs use menu:
Plugins->HangarOps UDP

Limitations:
------------
In a multiplayer scenario, players cannot select the same keycode with the
HangarOps menu. This will result in a hangar door animation that either does
not open or moves extremely slowly. In other words, HanagarOpsToSlave only
works using the concept of "private hangars".



Requirements:

1) slave computer designated for external visuals
2) identical airport scenery (with STMA hangars) installed on both master and
   slave
3) STMA Hangar Ops plugin installed on both master and slave
4) HangarOpsToSlave plugin installed ONLY on the master


Configuration:

The HangarOpsToSlave plugin root folder contains a file called slaveIP.ini.
Replace the loopback address (127.0.0.1) with the actual IP of the slave.
Failing to do this or entering an address which is not a valid IPv4 or IPv6
address will disable the sync feature. See log.txt in plugin root folder for
error messages.

Syncing:

The sync is achieved by the master sending UDP packets to the slave on
port 49000 in the following format:

DREF0<door position value><STMA dataref for STMA hangar door position>

For performance purposes these UDP packets are only sent to the slave if a
key code changes or the STMA hangar door is in transit.


Testing on a slave computer not running X-Plane:

Download SocketTest (http://sourceforge.net/projects/sockettest/) and install on
the slave. Configure UDP tab to IP address of slave and port 49000 and start
listening.

Version 2.0
===========
New Features
------------
1) Added support for multiple slave IPs


Version 1.1
===========
New Features
------------
1) OSX build now available in two options:
a) Built with clang statically linked to boost libs also built with clang.
   Dynamically linked to libc++ runtime.
b) Built with g++ statically linked to boost libs also built with g++.
   Dynamically linked to stdlibc++ v6 runtime.

2) Dependency on Microsoft C++ Redistributable removed for Windows build. Plugin
   is now statically linked to C++ runtime on Windows.

3) New Plugins menu "Hangar Ops UDP" which pops up a window showing real-time
   statistics of UDP packets sent to the slave IP addresses.

4) Improved error checking. HangarOpsToSlave now actively checks for STMA Hangar
   Ops plugin. If STMA Hangar Ops is not detected in 5 seconds HangarOpsToSlave
   disables itself with appropriate messages in Log.txt.

Bug Fixes
---------
1) Fixed critical bug in clean-up code which caused XP to crash intermittently
   on shutdown due to race condition between main and UDP service thread.
