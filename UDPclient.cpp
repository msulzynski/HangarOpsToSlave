/*
* UDPclient.cpp
* HangarOpsToSlave plugin
*
* version 2.0
*
* Created by Martin Sulzynski on 15/02/15.
* Copyright 2015 Martin Sulzynski. All rights reserved.
*
*
* This file is part of HangarOpsToSlave plugin
*
* HangarOpsToSlave is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version <http://www.gnu.org/licenses/>.
*
* HangarOpsToSlave is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* Prerequisites:
*
* STMA HangarOps installed on Master and any visual Slave PCs.
*
* Description:
*
* This is the implemenation for a UDP client designed to work
* with HangarOpsToSlave plugin.
*
* Memory management:
* The STMA data ref sent to the slave is managed via a boost::shared_ptr.
* Once the memory for cbuffer is allocated in UDPclient::send() it is
* sent to the Slave using async_send_to(). Since async_send_to() returns
* immediately and sends asynchronously its possible UDPclient::send() will
* return before the packet is sent. The shared_ptr is bound to the
* async_send_to() handler to ensure the shared_ptr is not deleted before the
* packet send has completed. Once the handler() returns, the last remaining
* reference to the shared pointer goes out of scope and the custom deleter
* is called to release the memory.
*/

#include "UDPclient.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <functional>               // for std::ref
#include <memory>                   // for std::shared_ptr
//#include <boost/smart_ptr/make_shared_array.hpp> //for make_shared with array

UDPclient::UDPclient(const boost::asio::ip::address & addr)
    : myDeleter(addr.to_string())
{
    endPt = boost::asio::ip::udp::endpoint(addr, 49000);
    socketPtr = new boost::asio::ip::udp::socket(io_service);
    socketPtr->open(boost::asio::ip::udp::v4());

    // work object associated with io_service keeps io_service alive
    workPtr = new boost::asio::io_service::work(io_service);

    // run io_service in separate thread so it does not block
    // running io_service is required otherwise when async_send_to() returns
    // it will not call its handler
    // if handler is not called it will not return so shared pointer ref on the
    // stack does not go out of scope and custom deleter is not called causing
    // a mem leak
    t = boost::thread(boost::bind(&boost::asio::io_service::run, &io_service));

    ipAddressStr = addr.to_string();
    packetsSent = 0;
    pointerCount = 0;
}

void UDPclient::send(char * data, size_t size)
{
    // shared_array is more appropriate here but shared_ptr allows
    // a custom deleter to be specified and debug messages can be
    // added to a custom deleter to confirm allocated memory is
    // being released - make_shared would be more efficient, but once
    // again has no custom deleter
    // Note: to allow myDeleter to maintain state between calls it must
    // be passed by reference with std::ref. The object also contains
    // a uint and string member so pass-by-value is not that cheap
    // given 1000 pointers are created for each door open/close operation
    std::shared_ptr<char> cbuffer(new char[size], std::ref(myDeleter));

    // more efficient alternative using make_shared, but no custom deleter
    // auto cbuffer = boost::make_shared<char[]>(size);

#ifndef NDEBUG
    if (cbuffer)
    {
        std::cout << "Allocated shared pointer " << ++pointerCount
                  << ". address " << static_cast<void*>(cbuffer.get())
                  << " for slave IP = " << ipAddressStr
                  << std::endl;
    }
#endif

    std::memcpy(cbuffer.get(), data, size);

    // async_send_to() is better than synchronous send_to() as it is non-blocking
    // so fps will not be affected if slave IP is not available
    //
    // a lambda expression serves as the handler required by async_send_to()
    // the lambda instantiates a functor and the captured cbuffer is copied
    // (by value) so shared_ptr ref count is incremented to manage cbuffer lifetime
    socketPtr->async_send_to(
        boost::asio::buffer((char *)cbuffer.get(),size), endPt,
        [cbuffer, this] (const boost::system::error_code&, std::size_t)
        {
#ifndef NDEBUG
            std::cout << "lambda: async_send_to() handler called for slave IP = "
                      << ipAddressStr << std::endl;
#endif
            packetsSent++; // requires this ptr to be captured by lambda
        }
    );
}

UDPclient::~UDPclient()
{
    // need to call the io_service object's stop() member function for
    // io_service run() call to return as soon as possible and terminate the
    // thread bound to io_service.
    // if io_service is not stopped here the thread is not cleaned up correctly
    // causing xplane to consistently crash on exit (File->Quit)
    io_service.stop();
    t.join();
    boost::system::error_code ec;
    socketPtr->shutdown(boost::asio::ip::tcp::socket::shutdown_send, ec);
#ifndef NDEBUG
    if (ec)
    {
        std::cout << "Error shutting down socket: error code:" << ec << std::endl;
    }
#endif
    socketPtr->close(ec);
#ifndef NDEBUG
    if (ec)
    {
        std::cout << "Error closing socket: error code:" << ec << std::endl;
    }
#endif
    delete socketPtr;
    delete workPtr;
#ifndef NDEBUG
    // For debug only. The destruct sequence ensures the body of
    // the destructor will execute first stopping the io_service and cleaning
    // up any remaining shared_ptrs with non-static member functor myDeleter
    // before calling myDeleter's destructor
    // If xplane starts to crash on exit all the shared_ptrs are may not be
    // gone by this point. This should not happen if io_service is stopped.
    std::cout << "Final statement in UDPclient destructor for " << ipAddressStr << std::endl;
#endif
}


