/*
* UDPclient.hpp
* HangarOpsToSlave plugin
*
* version 2.0
*
* Created by Martin Sulzynski on 15/02/15.
* Copyright 2015 Martin Sulzynski. All rights reserved.
*
*
* This file is part of HangarOpsToSlave plugin
*
* HangarOpsToSlave is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version <http://www.gnu.org/licenses/>.
*
* HangarOpsToSlave is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* Prerequisites:
*
* STMA HangarOps installed on Master and any visual Slave PCs.
*
* Description:
*
* This is the class definition for a UDP client designed to work
* with HangarOpsToSlave plugin. The network transport functionality
* is implemented with boost::asio.
*/

#ifndef UDP_CLIENT
#define UDP_CLIENT

#include <boost/asio.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/thread.hpp>
#include <boost/asio/io_service.hpp>

/*
 * Custom deleter functor for shared pointer pointing to char array
 */
class MyCustomDeleter
{
public:
    MyCustomDeleter(const std::string & addrStr)
        : m_addrStr(addrStr), deleterCount(0)
    {
#ifndef NDEBUG
        std::cout << "MyCustomDeleter constructor for " << addrStr << std::endl;
#endif
    }

// This is a loud copy constructor for debug
// It should never be called if the custom deleter is passed by reference std::ref()
    MyCustomDeleter(const MyCustomDeleter & rhs)
    {
#ifndef NDEBUG
        std::cout << "MyCustomDeleter copy constructor for " << m_addrStr << std::endl;
#endif
        m_addrStr=rhs.m_addrStr;
        deleterCount=rhs.deleterCount;
    }

    ~MyCustomDeleter()
    {
#ifndef NDEBUG
        std::cout << "MyCustomDeleter destructor for " << m_addrStr << std::endl;
#endif
    }

    void operator () (char* d)// can't be const since modifies deleterCount
    {
#ifndef NDEBUG
        std::cout << "Deleted shared pointer " << ++deleterCount
                  << ". address " << static_cast<void*>(d)
                  << " for slave IP = " << m_addrStr
                  << std::endl;
#endif
        delete [] d;
    }
private:
    std::string m_addrStr;      // to idetify the UDPclient object caller
    unsigned int deleterCount;  // track number of calls to functor
};

class UDPclient
{
public:
    UDPclient(const boost::asio::ip::address & addr);
    ~UDPclient();
    void send(char * data, size_t size);
    unsigned int getUDPpacketsSent() const {return packetsSent;}
    const char * getIPAddressStr() const {return ipAddressStr.c_str();}

private:
    MyCustomDeleter myDeleter;
    boost::asio::io_service io_service;
    boost::asio::ip::udp::endpoint endPt;
    boost::asio::ip::udp::socket* socketPtr;
    boost::thread t;
    boost::asio::io_service::work* workPtr;
    std::string ipAddressStr;
    unsigned int packetsSent;
    unsigned int pointerCount;
};

#endif
