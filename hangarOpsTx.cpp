/*
* hangarOpsTx.cpp
* HangarOpsToSlave plugin
*
* version 2.0
*
* Created by Martin Sulzynski on 13/02/15.
* Copyright 2015 Martin Sulzynski. All rights reserved.
*
*
* This file is part of HangarOpsToSlave plugin
*
* HangarOpsToSlave is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version <http://www.gnu.org/licenses/>.
*
* HangarOpsToSlave is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* Prerequisites:
*
* STMA HangarOps installed on Master and any visual Slave PCs.
*
* Description:
*
* This plugin gets the key code and door position of an STMA Hangar
* and sends the custom data ref as a UDP datagram packet to a
* Slave PC. When the pilot opens/closes a hangar door on the Master,
* the hangar door animation will also sync on the Slave.
*
* This plugin should be installed on the Master only.
*/

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define snprintf _snprintf
#endif

#include "XPLMPlugin.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"
#include "XPLMUtilities.h"
#include "XPLMMenus.h"

#include "XPLMDisplay.h"
#include "XPLMGraphics.h"

#include <fstream>                     // std::ifstream
#include <string>                      // std::string
#include <iostream>                    // std::cout
#include <sstream>                     // std::stringstream
#include <vector>
#include <set>
#include <memory>                      // for std::unique_ptr
#include <boost/algorithm/string.hpp>  // trim
#include <boost/tokenizer.hpp>         // to tokenize slave IPs
#include "UDPclient.hpp"

/*
* file scoped Global Variables.
*/

static XPLMDataRef stmaKeyCode = NULL;
static XPLMDataRef stmaDoorPos = NULL;
static std::vector<std::unique_ptr<UDPclient>> udpClientPtrs;

static float MyGetDataCallback
(
float inElapsedSinceLastCall,
float inElapsedTimeSinceLastFlightLoop,
int inCounter,
void * inRefcon
);

static float MyPluginDisableCallback
(
float inElapsedSinceLastCall,
float inElapsedTimeSinceLastFlightLoop,
int inCounter,
void * inRefcon
);

static float MyPluginInitializerCallback
(
float inElapsedSinceLastCall,
float inElapsedTimeSinceLastFlightLoop,
int inCounter,
void * inRefcon
);

static void initializeMenu();
static void MyMenuHandlerCallback( void * inMenuRef, void * inItemRef);

/* the window and its callbacks*/
static XPLMWindowID	udpWindow = NULL;

static void MyDrawWindowCallback(
                                   XPLMWindowID         inWindowID,
                                   void *               inRefcon);

static void MyHandleKeyCallback(
                                   XPLMWindowID         inWindowID,
                                   char                 inKey,
                                   XPLMKeyFlags         inFlags,
                                   char                 inVirtualKey,
                                   void *               inRefcon,
                                   int                  losingFocus);

static int MyHandleMouseClickCallback(
                                   XPLMWindowID         inWindowID,
                                   int                  x,
                                   int                  y,
                                   XPLMMouseStatus      inMouse,
                                   void *               inRefcon);


/*
* XPluginStart
*
* Our start routine registers our window and does any other initialization we
* must do.
*
*/
PLUGIN_API int XPluginStart(
char * outName,
char * outSig,
char * outDesc)
{
    /* describe plugin to the plugin-system. */
    strcpy(outName, "HangarOpsToSlave");
    strcpy(outSig, "msulga.hangaropstoslave");
    strcpy(outDesc, "Sends STMA hangar door data refs to slave PC");


    XPLMRegisterFlightLoopCallback(
                MyPluginDisableCallback, // Callback
                -1.0, // Interval
                NULL);
    return 1;
}

/*
* XPluginStop
*
* Our cleanup routine
*
*/
PLUGIN_API void XPluginStop(void)
{
    // since udpClientPtrs contains smart pointers
    // clearing the container automatically deallocates
    // then managed udpClient objects
    udpClientPtrs.clear();

    XPLMUnregisterFlightLoopCallback(
                MyGetDataCallback,
                NULL);
}

/*
* XPluginDisable
*
* We do not need to do anything when we are disabled, but we must provide the handler.
*
*/
PLUGIN_API void XPluginDisable(void)
{
}

/*
* XPluginEnable.
*
* We don't do any enable-specific initialization, but we must return 1 to indicate
* that we may be enabled at this time.
*
*/
PLUGIN_API int XPluginEnable(void)
{
    return 1;
}

/*
* XPluginReceiveMessage
*
* We don't have to do anything in our receive message handler, but we must provide one.
*
*/
PLUGIN_API void XPluginReceiveMessage(
XPLMPluginID inFromWho,
int inMessage,
void * inParam)
{
}

float MyGetDataCallback
(
float inElapsedSinceLastCall,
float inElapsedTimeSinceLastFlightLoop,
int inCounter,
void * inRefcon
)
{
    // get keycode and door position from STMA custom data refs
    int currentKeyCode = XPLMGetDatai(stmaKeyCode);
    float doorPosition = 0.0;
    XPLMGetDatavf(stmaDoorPos, &doorPosition, currentKeyCode, 1);

    // only send UDP packets to the slave if a hangar door
    // is in transit or a key code has changed
    // since floating point numbers are approximations
    // allow a 0.001 precision error for fully closed and open
    static int previousKeyCode = 0;
    if ((doorPosition < 0.001 || doorPosition > 0.999) &&
                     (currentKeyCode == previousKeyCode))
    {
        return -1;
    }

    previousKeyCode = currentKeyCode;

    // build the udpBuffer prefix
    char dref_prefix[5];
    strncpy(dref_prefix, "DREF0", 5);

    // build the STMA door position data ref based on the key code
    char dataRefStr[500];
    memset(dataRefStr,'\0',500);
    sprintf(dataRefStr, "STMA/Scenery/HangarDoorPosition[%d]", currentKeyCode);

    // build the udpBuffer for sending
    // DREF0<4 byte value><500 byte dref>
    char udpBuffer[509];
    std::memcpy(&udpBuffer[0], dref_prefix, 5);
    std::memcpy(&udpBuffer[5], &doorPosition, sizeof(float));
    std::memcpy(&udpBuffer[9], dataRefStr, sizeof(dataRefStr));

    // send UDP datagram
    if (!udpClientPtrs.empty())
    {
        for( const auto &j : udpClientPtrs )
        {
           j->send(&udpBuffer[0], sizeof(udpBuffer));;
        }
    }

    return -1.0;
}

/*
 * MyPluginDisableCallback
 *
 * This is the only flight loop call back registered by XPluginStart.
 * Its purpose is to detect STMA HangarOps before proceeding with
 * initialization of this plugin via MyPluginInitializerCallback.
 * If STMA HangarOps has not been detected after 5 seconds
 * MyPluginDisableCallback disables HangarOpsToSlave plugin.
 */
float MyPluginDisableCallback
(
float inElapsedSinceLastCall,
float inElapsedTimeSinceLastFlightLoop,
int inCounter,
void * inRefcon
)
{
    static int pluginCallCount = 0;

    /* before attempting to access STMA HangarOps DataRefs check
     * STMA plugin is loaded */
    XPLMPluginID hangarOpsID =
        XPLMFindPluginBySignature("BlueSideUpBob.STMA_HangarOps");
    if (hangarOpsID == XPLM_NO_PLUGIN_ID)
        hangarOpsID = XPLMFindPluginBySignature("BSUB.STMA_HangarOps");

    if (hangarOpsID == XPLM_NO_PLUGIN_ID)
    {
        XPLMDebugString("HangarOpsToSlave: STMA HangarOps plugin not found\n");
        if (pluginCallCount >= 5)
        {
            // no sign of STMA Hangar Ops plugin after 5 seconds
            XPLMDebugString("HangarOpsToSlave: Disabling HangarOpsToSlave\n");
            XPLMDisablePlugin(XPLMGetMyID());
        }
    }
    else
    {
        XPLMDebugString("HangarOpsToSlave: Found STMA HangarOps plugin\n");
        XPLMRegisterFlightLoopCallback(
                    MyPluginInitializerCallback, // Callback
                    //-1.0, // Interval
                    1.0, // 1 sec to allow STMA HangarOps to setup
                    NULL);
        return 0;
    }

    /* Call me every second to check if STMA HangarOps is loaded */
    pluginCallCount++;
    return 1.0;
}

/*
 * MyPluginInitializerCallback
 * This callback is registered by MyPluginDisableCallback if STMA HangarOps
 * is found. Its runs only once (returns 0) to set up resources.
 */
float MyPluginInitializerCallback
(
float inElapsedSinceLastCall,
float inElapsedTimeSinceLastFlightLoop,
int inCounter,
void * inRefcon
)
{
    /* no more need for MyPluginDisableCallback */
    XPLMUnregisterFlightLoopCallback(
                MyPluginDisableCallback,
                NULL);

    /* custom STMA data refs */
    stmaDoorPos = XPLMFindDataRef("STMA/Scenery/HangarDoorPosition");
    stmaKeyCode = XPLMFindDataRef("STMA/Remote/KeyCode");

    /* Register FlightLoop Callbacks */
    XPLMRegisterFlightLoopCallback(
                MyGetDataCallback, // Callback
                -1.0, // Interval
                NULL);

    // read slave IP from slaveIP.ini
    char pluginRoot[512];

    XPLMEnableFeature("XPLM_USE_NATIVE_PATHS",1);
    XPLMGetSystemPath(pluginRoot);
    strncat(pluginRoot, "Resources", sizeof(pluginRoot));
    strncat(pluginRoot, XPLMGetDirectorySeparator(), sizeof(pluginRoot));
    strncat(pluginRoot, "plugins", sizeof(pluginRoot));
    strncat(pluginRoot, XPLMGetDirectorySeparator(), sizeof(pluginRoot));
    strncat(pluginRoot, "HangarOpsToSlave", sizeof(pluginRoot));
    strncat(pluginRoot, XPLMGetDirectorySeparator(), sizeof(pluginRoot));

#ifndef NDEBUG
        std::cout << "plugin root path = " << pluginRoot << std::endl;
#endif

    char pluginINIfile[512];
    const char * INIfilename = "slaveIP.ini";
    strncpy(pluginINIfile, pluginRoot, sizeof(pluginINIfile));
    strncat(pluginINIfile, INIfilename, sizeof(pluginINIfile));
    std::ifstream infile(pluginINIfile);
#ifndef NDEBUG
        std::cout << "slave IP path = " << pluginINIfile << std::endl;
#endif

    char pluginLogfile[512];
    strncpy(pluginLogfile, pluginRoot, sizeof(pluginLogfile));
    strncat(pluginLogfile, "log.txt", sizeof(pluginLogfile));
    std::ofstream outfile(pluginLogfile);

#ifndef NDEBUG
        std::cout << "log file path = " << pluginLogfile << std::endl;
#endif

    boost::system::error_code ec;
    // using a set to store the slave IPs read from the ini file
    // since a set's keys are unique it will automatically handle
    // removal of any duplicate slave IPs in the ini file
    std::set<boost::asio::ip::address> slaveIPaddresses;

    if (infile.is_open())
    {
        std::stringstream buf;
        buf << infile.rdbuf();
        std::string slaveIPstr(buf.str());

        boost::char_separator<char> sep("\n");
        boost::tokenizer< boost::char_separator<char> > tokens(slaveIPstr, sep);

        for (std::string slaveIP : tokens)
        {
            boost::algorithm::trim(slaveIP);

            // boost from_string checks string is a valid IPv4 or IPv6 address
            auto slaveIPaddr = boost::asio::ip::address::from_string( slaveIP, ec );
            if ( ec )
            {
                if (outfile.is_open())
                {
                    outfile << ec.message( ) << std::endl;
                }
            }
            else if (!slaveIPaddr.is_loopback())
            {
                auto result = slaveIPaddresses.insert(slaveIPaddr);
                if (!(result.second))
                {
                    outfile << "Ignoring duplicate IP: " << slaveIPaddr.to_string() << std::endl;
                }
            }
        }

        for ( const auto &j : slaveIPaddresses )
        {
            outfile << "Valid IP: " << j.to_string() << std::endl;
        }

        infile.close();
    }
    else
    {
        if (outfile.is_open())
        {
            outfile << "Unable to open " << pluginINIfile << std::endl;
        }
    }

    if (slaveIPaddresses.empty())
    {
        if (outfile.is_open())
        {
            outfile << "No valid IPs. Disabling UDP datagram send." << std::endl
                      << "To enable UDP datagram send set (non-loopback) slave IP in " << INIfilename << std::endl;
        }
    }
    else // we have at least one valid IP address so create the UDPclient object
    {
        if (outfile.is_open())
        {
            outfile << "Creating " << slaveIPaddresses.size() << " UDP transport object"
                    << (slaveIPaddresses.size() > 1 ? "s":"") << std::endl;
        }

        for ( const auto &j : slaveIPaddresses )
        {
            std::unique_ptr<UDPclient> udpClientPtr (new UDPclient(j));

            if (udpClientPtr)
            {
                udpClientPtrs.push_back(std::move(udpClientPtr));
            }
        }
    }

    outfile.close();

    initializeMenu();

    udpWindow = XPLMCreateWindow(
                50, 600, 300, 575 - (21*udpClientPtrs.size()),			/* Area of the window. */
                0,							/* Start invisible. */
                MyDrawWindowCallback,		/* Callbacks */
                MyHandleKeyCallback,
                MyHandleMouseClickCallback,
                NULL);

    /* We only want the initializer callback to run once */
    return 0;
}

void initializeMenu()
{
    int mySubMenuItem = XPLMAppendMenuItem(
            XPLMFindPluginsMenu(),  /* Put in plugins menu */
            "Hangar Ops UDP",       /* Item Title */
            0,                      /* Item Ref */
            1);                     /* Force English */

    XPLMMenuID	myMenu =
        XPLMCreateMenu(
            "Hangar Ops UDP",
            XPLMFindPluginsMenu(),
            mySubMenuItem,            /* Menu Item to attach to. */
            MyMenuHandlerCallback,    /* The handler */
            0);                       /* Handler Ref */

    XPLMAppendMenuItem(myMenu, "Show UDP Stats", (void *)"Show UDP Stats", 1);
    XPLMAppendMenuItem(myMenu, "Hide UDP Stats", (void *)"Hide UDP Stats", 1);

}

void MyMenuHandlerCallback( void * inMenuRef, void * inItemRef)
{
    if (!strcmp((char *) inItemRef, "Show UDP Stats"))
    {
        XPLMSetWindowIsVisible(udpWindow,1);
    }
    else if (!strcmp((char *) inItemRef, "Hide UDP Stats"))
    {
        XPLMSetWindowIsVisible(udpWindow,0);
    }
}

/*
 * MyDrawingWindowCallback
 *
 * This callback does the work of drawing our window once per sim cycle each time
 * it is needed.  It dynamically changes the text depending on the saved mouse
 * status.  Note that we don't have to tell X-Plane to redraw us when our text
 * changes; we are redrawn by the sim continuously.
 *
 */
void MyDrawWindowCallback(
                                   XPLMWindowID         inWindowID,
                                   void *               inRefcon)
{
    int		left, top, right, bottom;
    float	color[] = { 1.0, 1.0, 1.0 }; 	/* RGB White */

    /* First we get the location of the window passed in to us. */
    XPLMGetWindowGeometry(inWindowID, &left, &top, &right, &bottom);

    /* We now use an XPLMGraphics routine to draw a translucent dark
     * rectangle that is our window's shape. */
    XPLMDrawTranslucentDarkBox(left, top, right, bottom);

    char buffer [100];
    unsigned line_no = 0;
    if (!udpClientPtrs.empty())
    {
        for ( const auto &j : udpClientPtrs )
        {
            snprintf(buffer, sizeof(buffer), "%u ==> %s",
                j->getUDPpacketsSent(), j->getIPAddressStr());
            XPLMDrawString(color, left + 5, top - 20 - (++line_no * 20),
                buffer, NULL, xplmFont_Basic);
        }
    }
    else
    {
        snprintf(buffer, sizeof(buffer), "UDP send disabled.");
    }

    /* Finally we draw the text into the window, also using XPLMGraphics
     * routines.  The NULL indicates no word wrapping. */
    const char * prefix = "UDP Packets sent to:";
    XPLMDrawString(color, left + 5, top - 20,
        const_cast<char *>(prefix), NULL, xplmFont_Basic);


}

/*
 * MyHandleKeyCallback
 *
 * Currently does nothing
 */
void MyHandleKeyCallback(
                                   XPLMWindowID         inWindowID,
                                   char                 inKey,
                                   XPLMKeyFlags         inFlags,
                                   char                 inVirtualKey,
                                   void *               inRefcon,
                                   int                  losingFocus)
{
}

/*
 * MyHandleMouseClickCallback
 *
 * Currently does nothing
 */
int MyHandleMouseClickCallback(
                                   XPLMWindowID         inWindowID,
                                   int                  x,
                                   int                  y,
                                   XPLMMouseStatus      inMouse,
                                   void *               inRefcon)
{
    return 1;
}
