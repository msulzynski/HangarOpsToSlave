# Shared library without any Qt functionality
TEMPLATE = lib
QT -= gui core

CONFIG += warn_on plugin
CONFIG -= thread exceptions qt rtti

#TODO: CONFIG += exceptions for mac and unix builds

VERSION = 1.1.0

INCLUDEPATH += ../SDK/CHeaders/XPLM
INCLUDEPATH += ../SDK/CHeaders/Wrappers
INCLUDEPATH += ../SDK/CHeaders/Widgets

# Defined to use X-Plane SDK 2.1 capabilities - no backward compatibility before 9.0
DEFINES += XPLM200
DEFINES += XPLM210

win32 {
    #tweak Qt to statically link in the C++ runtime library
    #For Qt 5.4, you need to edit mkspecs\win32-msvc2013\qmake.conf and replace in the CFLAGS sections '-MD' with '-MT':
    #QMAKE_CFLAGS_RELEASE    = -O2 -MT -Zc:strictStrings
    #QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO += -O2 -MT -Zi -Zc:strictStrings
    #QMAKE_CFLAGS_DEBUG      = -Zi -MTd

    CONFIG += exceptions
    boost_prefix=E:\local\boost_1_59_0
    INCLUDEPATH += $${boost_prefix}\include\boost-1_59
    DEFINES += APL=0 IBM=1 LIN=0
    LIBS += -L../SDK/Libraries/Win


    contains(QT_ARCH, x86_64) {
        #boost libs built with link=static runtime-link=static
        LIBS += -L$${boost_prefix}\lib64-msvc-12.0
        LIBS += -lXPLM_64 -lXPWidgets_64
    }

    contains(QT_ARCH, i386) {
        #boost libs built with link=static runtime-link=static
        LIBS += -L$${boost_prefix}\lib32-msvc-12.0
        LIBS += -lXPLM -lXPWidgets
    }

    CONFIG(debug, debug|release) {
        LIBS += -llibboost_system-vc120-mt-sgd-1_59
        LIBS += -llibboost_thread-vc120-mt-sgd-1_59
    } else {
        LIBS += -llibboost_system-vc120-mt-s-1_59
        LIBS += -llibboost_thread-vc120-mt-s-1_59
    }

    TARGET = win.xpl

}

unix:!macx {
    DEFINES += APL=0 IBM=0 LIN=1

    CONFIG += exceptions
    QMAKE_CXXFLAGS += -std=c++11

    boost_prefix=$(HOME)/boost_1_59_0

    INCLUDEPATH += $${boost_prefix}/include

    CONFIG(debug, debug|release) {
        DEFINES -= NDEBUG
        dbg_suffix=-dbg
    } else {
        DEFINES += NDEBUG
    }

    if (linux-g++) {
        address_model=64
        QMAKE_LFLAGS += -nodefaultlibs
    }

    if (linux-g++-32) {
        address_model=32
        #QMAKE_CXXFLAGS += -m32
        #QMAKE_LFLAGS += -m32
        #QMAKE_LFLAGS += -nodefaultlibs
    }

    libs_prefix=$${boost_prefix}/lib$${address_model}$${dbg_suffix}
    message (lib prefix = $${libs_prefix})

    LIBS += $${libs_prefix}/libboost_system.a
    LIBS += $${libs_prefix}/libboost_thread.a

    TARGET = lin.xpl
    # WARNING! This requires the latest version of the X-SDK !!!!
    QMAKE_CXXFLAGS += -fvisibility=hidden
    QMAKE_LFLAGS += -shared -rdynamic -undefined_warning #-nodefaultlibs
    QMAKE_LFLAGS += -Wl,--version-script=$(HOME)/xplugins/hangarOpsTx/version_script
}

macx {
    DEFINES += APL=1 IBM=0 LIN=0
    TARGET = mac.xpl
    QMAKE_LFLAGS += -F../SDK/Libraries/Mac
    LIBS += -framework XPLM
    LIBS += -framework XPWidgets

    CONFIG += plugin
    QMAKE_LFLAGS_PLUGIN -= -dynamiclib
    QMAKE_LFLAGS_PLUGIN += -bundle
    QMAKE_LFLAGS += "-exported_symbols_list $(HOME)/Documents/xplugins/hangarOpsTx/ld_script"

    CONFIG(debug, debug|release) {
        message ("debug")
        DEFINES -= NDEBUG
    } else {
        message ("release")
        DEFINES += NDEBUG
    }

    # do we need these extra flags here?
    QMAKE_CXXFLAGS += -fvisibility-inlines-hidden
    QMAKE_CXXFLAGS += -fvisibility=hidden
    QMAKE_MACOSX_DEPLOYMENT_TARGET=10.6

    boost_prefix=$(HOME)/boost_1_59_0

    INCLUDEPATH += $${boost_prefix}/include

    if (macx-clang) {
        QMAKE_LFLAGS += -stdlib=libc++
        QMAKE_CXXFLAGS += -stdlib=libc++
        QMAKE_CXXFLAGS += -std=c++11

        CONFIG(x86_64, x86_64|x86) {
            message ("clang 64-bit build")
            #built with clang and libc++
            LIBS += $${boost_prefix}/lib64/libboost_system.a
            LIBS += $${boost_prefix}/lib64/libboost_thread.a
        }

        CONFIG(x86, x86_64|x86) {
            message ("clang 32-bit build")
            #built with clang and libc++
            LIBS += $${boost_prefix}/lib32/libboost_system.a
            LIBS += $${boost_prefix}/lib32/libboost_thread.a
        }
    }

    if (macx-g++) {
        QMAKE_CXXFLAGS += -std=c++11
        QMAKE_CFLAGS_X86_64 -= -Xarch_x86_64
        QMAKE_CXXFLAGS_X86_64 -= -Xarch_x86_64
        QMAKE_LFLAGS_X86_64 -= -Xarch_x86_64
        QMAKE_CXX=/opt/local/bin/g++-mp-4.9
        QMAKE_CC=/opt/local/bin/gcc-mp-4.9
        QMAKE_LINK=/opt/local/bin/g++-mp-4.9
        message ("g++ 32/64-bit macports build")
        #fat 32/64 bit boost libs built with gcc 4.2 and stdlib
        LIBS += /opt/local/lib/libboost_system-mt.a
        LIBS += /opt/local/lib/libboost_thread-mt.a
    }
}

HEADERS += \
    UDPclient.hpp

SOURCES += \
    UDPclient.cpp \
    hangarOpsTx.cpp
